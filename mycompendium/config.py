import argparse
import os

__parser = argparse.ArgumentParser()

__parser.add_argument('-i', '--input', action='append', required=True)
__parser.add_argument('-t', '--template', required=True)
__parser.add_argument('-o', '--output')

__args = __parser.parse_args()

template = __args.template

inputs = []
for i in __args.input:
    name, extension = os.path.splitext(i)
    if extension not in ['.yml', '.yaml']:
        i += '.yml'
    if not os.path.isfile(i):
        print "Could not find input file %s"%i
    else:
        inputs.append(i)

output = __args.output

