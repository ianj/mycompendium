from jinja2 import Environment, FileSystemLoader
from vendor.yamlapp import yamlapp
from mycompendium import config
from copy import deepcopy

class Builder:
    context = dict()
    template = None

    def __init__(self):
        for input_file in config.inputs:
            data = yamlapp.parseFile(input_file)
            self.context = self._meld(self.context, data)

        # packageloader would be useful for theming, ie loading templates from other eggs/packages?
        # package_loader = PackageLoader('theme', 'templates')  # 'templates' folder in __main__ module
        env = Environment(loader=FileSystemLoader('./templates'))
        self.template = env.get_template(config.template)

    def _meld(self, a, b):
        """
        will recurse into sub dicts
        if key is in both a and b _meld is called again adding b to a
        if key is in both and it's a list a's list is extended.
        TODO: 90% sure this is bug riddled, written at 3am, will need tests.
        """
        if not isinstance(b, dict):
            return b
        result = deepcopy(a)
        for k, v in b.iteritems():
            if k in result and isinstance(result[k], dict):
                result[k] = self._meld(result[k], v)
            elif k in result and isinstance(result[k], list):
                result[k].extend(v)
            else:
                result[k] = deepcopy(v)
        return result

    def render(self):
        txt = self.template.render(self.context)
        with open(config.output, "wb") as f:
            f.write(txt)


