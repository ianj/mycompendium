# MyCompendium

### What?
Is an experiment in generating résumés / CVs. 

__NB__ : There are many, MANY, better tools for generating CV's. For example this is inspired by HackMyResume.

### Why?

Want to source control CV info, having it in yaml format is easy.
Multiple inputs can be combined into one output document.
It shouldn't be too hard to add i18n which I might need in the future to make mandarin/english versions.
Easy make an html document and easier to save/export it as a PDF.


### How?

store CV data in one or more yaml files which can be selectivly chosen to pass into the builder with combines the data into a context to give to the jinja2 template engin to make an html document.

### Examples:

    ./main.py -i config/example.yml -t compact.html -o example.html
    
### Goals:

**These are Goals, ie Not working or work in progress 

Mix 'n Match input sources

    ./main.py -i base.yml -i web_skills.yml -i recent_histoy.yml -t compact -o for_a_web_placement.html
    
I18N

    ./main.py -i example.py -l zh_Tw -t compact -o for_a_tw_position.html


